var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Validation Pass Test', success: req.session.success, errors: req.session.errors});
  req.session.errors = null;
});

router.post('/submit', function (req, res, next){
   req.check('mail', 'The e-mail is incorrect').isEmail();
   req.check('pass', 'The password is incorrect').isLength({min: 6, max: 24}).equals(req.body.Cpass);
   var errors = req.validationErrors();
   if(errors) {
       req.session.errors = errors;
       req.session.success = false;
   } else {
       req.session.success = true;
   }

   res.redirect('/');


});



module.exports = router;
