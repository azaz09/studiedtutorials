var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'My APP', someArray: [1,2,3,4,5]});
});

router.get('/test/:id', function(req, res, next){
    res.render('test', {title: 'Show me The id, sir', id: req.params.id});
});

router.get('/test/:id/:a', function(req, res, next) {
    res.render('test', {title: 'My APP', id: req.params.id, answer: req.params.a})
});

router.post('/test/submit', function(req, res, next){
   var id= req.body.id;
   if(id) res.redirect('/test/'+id);
   else res.redirect('/');
});



module.exports = router;
