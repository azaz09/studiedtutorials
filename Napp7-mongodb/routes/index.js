var express = require('express');
var router = express.Router();
var assert = require('assert');
var url = "mongodb://localhost:27017";
const MongoClient = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
const client = new MongoClient(url);
const dbName = 'test';

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/get-data', function(req, res, next){
  var resultArray = [];
  MongoClient.connect(url, function(err, client){
    assert.equal(null, err);
    const db = client.db(dbName);
    var cursor = db.collection('user-data').find();
    cursor.forEach(function(doc, err) {
        assert.equal(null, err);
        resultArray.push(doc);
    }, function(){
      client.close();
      res.render('index', {items: resultArray});
    });
  });
});

router.post('/insert', function(req, res, next) {
  var item = {
    title: req.body.title,
    content: req.body.content,
    author: req.body.author
  }
  MongoClient.connect(url, function(err, client){
    assert.equal(null, err);
    const db = client.db(dbName);
    db.collection('user-data').insertOne(item, function(err, result){
      assert.equal(null, err);
      console.log('Item inserted');
      client.close();
    });
  });
  res.redirect('/get-data');
});

router.post('/update', function(req, res, next) {
    var item = {
        title: req.body.title,
        content: req.body.content,
        author: req.body.author
    }
    var id = req.body.id;
    MongoClient.connect(url, function(err, client){
        assert.equal(null, err);
        const db = client.db(dbName);
        db.collection('user-data').updateOne({"_id": objectId(id)},{$set: item}, function(err, result){
            assert.equal(null, err);
            console.log('Item updated');
            client.close();
        });
    });
    res.redirect('/get-data');
});

router.post('/delete', function(req, res, next) {
    var id = req.body.id;

    MongoClient.connect(url, function(err, client){
        assert.equal(null, err);
        const db = client.db(dbName);
        db.collection('user-data').deleteOne({"_id": objectId(id)}, function(err, result){
            assert.equal(null, err);
            console.log('Item deleted');
            client.close();
        });
    });
    res.redirect('/get-data');
});

module.exports = router;
