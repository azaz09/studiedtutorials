var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'My APP', someArray: [1,2,3,4,5]});
});

router.get('/test/:id/:a', function(req, res, next) {
    res.render('test', {o_id: req.params.id, answer: req.params.a})
});



module.exports = router;
