import React from 'react';
import PropTypes from 'prop-types';
import styles from './Title.module.scss';

const Title = ({children, tag: Tag}) => (
    <Tag className={styles.title}>
        {children}
    </Tag>
)

Title.propTypes = {
    tag: PropTypes.string.isRequired,
}

export default Title;
