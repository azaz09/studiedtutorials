import React from 'react';
import styles from './ListItem.module.scss';
import PropTypes from 'prop-types';
import Button from '../../Button/Button';
import Title from '../../Title/Title';

const ListItem = ({
    image, name, description, twitterLink}) => {
    const ImageTag = image ? "img" : "div";
    return (
        <li className={styles.wrapper}>
            <ImageTag src={image} className={image ? styles.image : styles.imageNone} alt={name} />
            <div>
                <Title tag="h2">{name}</Title>
                <p className={styles.description}>
                    {description}
                </p>
                <Button href={twitterLink}>Twitter Page</Button>
            </div>
        </li>
        )
};

ListItem.propTypes = {
    image: PropTypes.string,
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    twitterLink: PropTypes.string.isRequired,
}

ListItem.defaultProps = {
    image: null,
    description: 'One of the React Creator',
}
export default ListItem;