import React from 'react';
import styles from './Button.module.scss';

const Button = ({children, href}) => (
    <>
    { href ? (
            <a className={styles.button}
               href={href}
               rel='noopener noreferrer' target='_blank'>{children}</a>
        ) : (
                <button className={styles.button}>
                    {children}
                </button>
            )
    }
    </>
);

export default Button;