const express = require('express');
const router = express.Router();
const checkAuth = require("../middleware/checkAuth");
const userController = require("../controllers/userController");

router.post("/signup", checkAuth, userController.createUser);

router.post("/login", checkAuth, userController.logIn);

router.delete("/:userId", checkAuth, userController.deleteUser);

module.exports = router;