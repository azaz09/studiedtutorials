const express = require('express');
const router = express.Router();

const productsController = require("../controllers/productsController");

const multer = require("multer");
const checkAuth = require("../middleware/checkAuth");
const storage = multer.diskStorage({
   destination: function(req, file, cb){
        cb(null, "./uploads");
   },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString()+file.originalname);
    }
});

const fileFilter = (req, file, cb) =>{
//reject a file
    if(file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
        storage: storage,
    limits: {
            fileSize: 1024 * 1024 * 6
    },
    fileFilter: fileFilter

});


router.get('/', productsController.getAll);

router.post('/', checkAuth, upload.single("productImg"), productsController.createProduct);

router.get('/:productId', productsController.getOne);

router.patch('/:productId', checkAuth, productsController.updateProduct);

router.delete('/:productId', checkAuth, productsController.deleteProduct);

module.exports = router;