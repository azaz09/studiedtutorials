const express = require('express');
const router = express.Router();

const checkAuth = require("../middleware/checkAuth");
const ordersController = require("../controllers/ordersController");

router.get('/', checkAuth, ordersController.getAll);

router.post('/', checkAuth, ordersController.createOrder);


router.get('/:orderId', checkAuth, ordersController.getOne);


router.delete('/:orderId', checkAuth, ordersController.deleteOrder);

module.exports = router;