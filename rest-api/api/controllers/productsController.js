const mongoose = require('mongoose');

const Product = require("../models/product.js");


exports.getAll = (req, res, next) => {
    Product.find().select("name price _id productImg")
        .exec()
        .then(docs =>{
            const response = {
                count: docs.length,
                products: docs.map(doc => {
                    return {
                        name: doc.name,
                        price: doc.price,
                        productImage: doc.productImg,
                        id: doc._id,
                        request: {
                            type: "GET",
                            url: process.request.url+"/products/"+doc._id
                        }
                    }
                })
            };
            res.status(200).json(response);
        })
        .catch(err=>{
            res.status(500).json({
                error: err
            });
        });
};

exports.createProduct = (req, res, next) => {

    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        productImg: req.file.path
    });
    product.
    save().
    then(result =>{
        res.status(201).json({
            message: "Created product successfully",
            Created: {
                name: result.name,
                price: result.price,
                id: result._id,
                request: {
                    type: "POST",
                    url: process.request.url+"/products/"+result._id
                }
            }
        });
    }).catch(err=> {
        console.log(err);
        res.status(500).json({error: err});
    });
};

exports.getOne = (req, res, next) => {
    const id = req.params.productId;
    Product.findById({_id: id}).select("name price _id productImg")
        .exec()
        .then(doc=> {
            if(doc) {
                res.status(200).json({
                        name: doc.name,
                        price: doc.price,
                        productImage: doc.productImg,
                        id: doc._id,
                        request: {
                            type: "GET",
                            url: process.request.url+"/products/"+doc._id
                        }
                    }
                );
            } else {
                res.status(404).json({
                    message: "Not valid entry found for provided ID"
                });
            }
        })
        .catch(err=> {
            console.log(err);
            res.status(500).json({error: err});
        });
};

exports.updateProduct = (req, res, next) => {
    const id = req.param.productId;
    const updateOps = {};
    for(const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    Product.update({_id: id}, {$set: updateOps}).select("name price _id")
        .exec()
        .then(result => {
            res.status(200).json({
                Updated: {
                    name: result.name,
                    price: result.price,
                    id: result._id,
                    request: {
                        type: "PATCH",
                        url: process.request.url+"/products/"+result._id
                    }
                }
            });
        })
        .catch(err =>{
            console.log(err);
            res.status(500).json({error: err});
        });
};

exports.deleteProduct = (req, res, next) => {
    const id = req.param.productId;
    Product.remove({_id: id}).select("name price _id")
        .exec()
        .then(docs =>{
            const response = {
                count: docs.length,
                Deleted: {
                    name: doc.name,
                    price: doc.price,
                    id: doc._id,
                    request: {
                        type: "POST",
                        url: process.request.url+"/products/"+doc._id,
                        body: {name: 'string', price: "Number"}
                    }
                }

            };
            res.status(200).json(response)
        })
        .catch(err=>{
            res.status(500).json({
                error: err
            });
        });
};