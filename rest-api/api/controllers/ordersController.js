const mongoose = require('mongoose');

const Order = require("../models/order");
const Product = require("../models/product");

exports.getAll = (req, res, next) => {
    Order.find().select("product quantity _id")
        .exec()
        .then(docs => {
            res.status(200).json({
                count: docs.length,
                orders: docs.map(doc =>{
                    return {
                        product: doc.product,
                        quantity: doc.quantity,
                        id: doc._id,
                        request: {
                            type: "GET",
                            url: process.request.url + "/orders/"
                        }
                    }
                })
            });
        })
        .catch(err => {
            res.status(404).json({
                Error: err
            })
        })
};

exports.createOrder = (req, res, next) => {
    Product.findById(req.body.productId).exec()
        .then(product => {
            if(!product) res.status(404).json({message: "Product not Found"});
            const order = new Order({
                _id: mongoose.Types.ObjectId(),
                quantity: req.body.quantity,
                product: req.body.productId
            });
            return order.save();
        })
        .then(result=>{
            res.status(201).json({
                message: 'Order was created!',
                messPL: 'Zamówienie zostało stworzone!',
                createdOrder: {
                    id: result._id,
                    quantity: result.quantity
                },
                request: {
                    type: 'POST',
                    url: process.request.url+"/orders/"+result._id
                }
            });
        })
        .catch(err => {
            res.status(500).json({
                Error: err
            });
        });
};

exports.getOne = (req, res, next) => {
    Order.findById(req.params.orderId).select("order quantity _id")
        .exec()
        .then(order => {
            if(!order) {
                res.status(404).json({
                    message: "Order not found"
                });
            }
            res.status(200).json({
                order: order._id,
                request: {
                    type: "GET",
                    url: process.request.url+"/orders/"+order._id
                }
            });
        })
        .catch(err=>{
            res.status(500).json({
                Error: err
            })
        });
};

exports.deleteOrder = (req, res, next) => {
    Order.remove({_id: req.params.orderId}).exec()
        .then(order=> {
            res.status(200).json({
                message: "Deleted successfully",
                Deleted: {
                    order: order._id,
                    request: {
                        type: "POST",
                        url: process.request.url+"/orders/"+order._id,
                        body: {productId: 'ID', quantity: 'Number' }
                    }
                }
            })
        })
        .catch(err=>{
            res.status(500).json({
                Error: err
            })
        });
};