const express = require("express");
const app = express();
const morgan = require('morgan');
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const productRoutes = require('./api/routes/products');
const orderRoutes = require('./api/routes/orders');
const userRoutes = require('./api/routes/user.js');
mongoose.connect(
    "mongodb://node-rest:"+
    process.env.Pass+
    "@node-rest-shard-00-00-emd1j.mongodb.net:27017,node-rest-shard-00-01-emd1j.mongodb.net:27017,node-rest-shard-00-02-emd1j.mongodb.net:27017/test?ssl=true&replicaSet=node-rest-shard-0&authSource=admin&retryWrites=true"
    ,{
        useNewUrlParser: true
    }
);
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use("/uploads", express.static("uploads"))
//Routes to handling requests
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);
app.use('/user', userRoutes);

app.use((req, res, next) => {
   res.header('Access-Control-Allow-Origin', '*');
   res.header(
     "Access-Control-Allow-Headers",
       "Origin, X-Requested-With, Content-Type, Accept, Authorization"
   );

   if(req.method === "OPTIONS") {
       res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, PATCH, DELETE');
       return res.status(200).json();
   }
   next();
});

app.use((req, res, next) => {
   const error = new Error('Error -> not found');
   error.status = 404;
   next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
       error: {
           message: error.message
       }
    });
});

module.exports = app;
