var url = require("url");

function renderHTML(path, response) {
    fs.readFile(path, null, function(error, data) {
        if(error) {
            response.writeHead(404);
            response.write("Sorry, That page doesn't exist.. curently");
        } else {
            response.write(data);
        }
        response.end();
    });
}

module.exports = {
    handleRequest : function(request, response) {
        response.writeHead("200", {"Content-type": "text/html"});
        var path = url.parse(request.url).pathname;
        switch(path) {
            case "/":
                renderHTML("./index.html", response);
            break;
            case "/logon":
                renderHTML("./logon.html", response);
                break;
            default:
                response.writeHead(404);
                response.write("Sorry, That page doesn't exist.. curently");
                response.end();
        }
    }
}