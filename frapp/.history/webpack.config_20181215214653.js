const path = require('path');

module.exports = {
  entry: './src/home.js',
  output: {
    path: __dirname + "/src",
    filename: 'bundle.js'
  }
};