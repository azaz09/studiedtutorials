const path = require('path');

module.exports = {
  entry: {
    home: './src/home.js', 
    post: './src/post.js',
    contact: './src/contact.js'
    }
  output: {
    path: __dirname,
    filename: 'bundle.js'
  }
};