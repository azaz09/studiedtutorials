var http = require('http');

function onRequest(request, response) {
    response.writeHead({"Content-type":"text/plain"});
    response.write("Tabula raza");

    response.end();
}

http.createServer(onRequest).listen(8000);

