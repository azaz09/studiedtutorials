var http = require('http');

var module1 = require("./module/module1.js");
var module2 = require("./module/module2.js");

function onRequest(request, response) {
    response.writeHead(200, {"Content-type":"text/plain"});
    response.write("Tabula raza");
    response.write(module1.str);
    response.end();
}

http.createServer(onRequest).listen(8000);

